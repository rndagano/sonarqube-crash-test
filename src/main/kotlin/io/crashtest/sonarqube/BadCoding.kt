package io.crashtest.sonarqube

import org.slf4j.LoggerFactory

class BadCoding {
	private val logger = LoggerFactory.getLogger(this.javaClass)

	fun longIfElse(int: Int): String {
		if (int == 0)
			return "Zero"
		else if (int == 1)
			return "One"
		else if (int == 2)
			return "Two"
		else if (int == 3)
			return "Three"
		else if (int == 4)
			return "Four"
		else if (int == 5)
			return "Five"
		else if (int == 6)
			return "Six"
		else if (int == 7)
			return "Seven"
		else if (int == 8)
			return "Eight"
		else if (int == 9)
			return "Nine"
		else if (int == 10)
			return "Ten"
		return "Nil"
	}

	fun longIfElseDuplicate(int: Int): String {
		if (int == 0)
			return "Zero"
		else if (int == 1)
			return "One"
		else if (int == 2)
			return "Two"
		else if (int == 3)
			return "Three"
		else if (int == 4)
			return "Four"
		else if (int == 5)
			return "Five"
		else if (int == 6)
			return "Six"
		else if (int == 7)
			return "Seven"
		else if (int == 8)
			return "Eight"
		else if (int == 9)
			return "Nine"
		else if (int == 10)
			return "Ten"
		return "Nil"
	}
}