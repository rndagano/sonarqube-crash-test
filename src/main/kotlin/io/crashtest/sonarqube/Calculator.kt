package io.crashtest.sonarqube

class Calculator {
	fun add(a: Int, b: Int): Int {
		return a + b
	}

	fun addition(a: Int, b: Int): Int {
		return a + b
	}

	fun sub(a: Int, b: Int): Int {
		return a - b
	}

	fun multiply(a: Int, b: Int): Int {
		return a * b
	}

	fun square(a: Int, b: Int): Int {
		return a * b
	}

	fun divide(a: Int, b: Int): Int {
		return a / b
	}
}
