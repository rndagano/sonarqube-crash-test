package io.crashtest.sonarqube

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CalculatorTest {
	private lateinit var calculator: Calculator

	@BeforeEach
	fun setUp() {
		calculator = Calculator()
	}

	@Test
	fun add() {
		assertEquals(30, calculator.add(10, 20))
	}
}