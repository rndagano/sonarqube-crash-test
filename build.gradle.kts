import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import io.gitlab.arturbosch.detekt.Detekt

plugins {
    id("org.springframework.boot") version "2.4.5"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.5.0"
    kotlin("plugin.spring") version "1.5.0"
    id("org.sonarqube") version ("3.2.0")
    jacoco
    id("io.gitlab.arturbosch.detekt").version("1.17.0")
}

jacoco {
    toolVersion = "0.8.7"
}

group "io.crashtest.sonarqube"
java.sourceCompatibility = JavaVersion.VERSION_15

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.32")
    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "15"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
        html.isEnabled = false
    }
}

tasks.jacocoTestReport {
    dependsOn(tasks.detekt) // tests are required to run before generating the report
}

/*tasks.withType<Detekt>().configureEach {
    // Target version of the generated JVM bytecode. It is used for type resolution.
    this.jvmTarget = "1.8"
}*/

detekt {
    toolVersion = "1.17.0"
    ignoreFailures = true

    reports {
        // Enable/Disable XML report (default: true)
        xml {
            enabled = true
            destination = file("build/reports/detekt.xml")
        }
        // Enable/Disable HTML report (default: true)
        html {
            enabled = true
            destination = file("build/reports/detekt.html")
        }
        // Enable/Disable TXT report (default: true)
        txt {
            enabled = true
            destination = file("build/reports/detekt.txt")
        }
    }
}

tasks.sonarqube{
    dependsOn(tasks.test)
}

sonarqube {
    properties {
        property("sonar.sourceEncoding", "UTF-8")
        property("sonar.projectKey", "rndagano_sonarqube-crash-test")
        property("sonar.organization", "rndagano-tests")
        //FIXME: replace 'build/reports/detekt.xml' path by a variable
        property("sonar.kotlin.detekt.reportPaths", "build/reports/detekt.xml")
        //detekt.reports.xml.destination?.let { property("sonar.kotlin.detekt.reportPaths", it) }
    }
}